/*
 *  Copyright 2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.se.parse;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.lucene.document.Field;

import java.util.ArrayList;

/**
 * Represents a parsed document to be indexed.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
public class ParsedDocument {

    /**
     * The names of the {@link Field}s within the index.
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    public final static class FIELDS {

        /**
         * The document identifier
         */
        public static final String ID = "id";

        /**
         * The document body
         */
        public static final String BODY = "body";
        /**
         * The document score
         */
        public static final String SCORE = "score";
        /**
         * The document views
         */
        public static final String VIEWS = "viewCount";
        /**
         * The document favourite
         */
        public static final String FAVOURITE = "favouriteCount";
        /**
         * The document title
         */
        public static final String TITLE = "title";
        /**
         * The document tags
         */
        public static final String TAGS = "tags";
        /**
         * The document post type
         */
        public static final String POSTTYPE = "postType";
        /**
         * The document post type
         */
        public static final String FORMULAID = "formula_IDs";
    }


    /**
     * The unique document identifier.
     */
    private final String id;

    /**
     * The body of the document.
     */
    private final String body;
    // New Fields
    private final String score;
    private final String viewCount;
    private final String title;
    private final String tags;
    private final String postType;
    private final String favouriteCount;
    private final ArrayList<Integer> formula_IDs;

    ///
    /**
     * Creates a new parsed document
     *
     * @param id   the unique document identifier.
     * @param body the body of the document.
     * @throws NullPointerException  if {@code id} and/or {@code body} are {@code null}.
     * @throws IllegalStateException if {@code id} and/or {@code body} are empty.
     */
    public ParsedDocument(final String id, final String body, final String score, final String viewCount, final String title,
                          final String tags, final String postType, final String favouriteCount, final ArrayList<Integer> formula_IDs) {

        if (id == null) {
            throw new NullPointerException("Document identifier cannot be null.");
        }

        if (id.isEmpty()) {
            throw new IllegalStateException("Document identifier cannot be empty.");
        }

        this.id = id;

        if (body == null) {
            throw new NullPointerException("Document body cannot be null.");
        }

        if (body.isEmpty()) {
            throw new IllegalStateException("Document body cannot be empty.");
        }

        this.body = body;

        if (score == null) {
            throw new NullPointerException("Document score cannot be null.");
        }

        if (score.isEmpty()) {
            throw new IllegalStateException("Document score cannot be empty.");
        }

        this.score = score;

        if (viewCount == null) {
            this.viewCount = "0";
        }

        else if (viewCount.isEmpty()) {
            this.viewCount = "0";
        }
        else this.viewCount = viewCount;

        if (title == null) {
            this.title = "0";
        }
        else if (title.isEmpty()) {
            this.title = "0";
        }
        else{
            this.title = title;
        }

        if (tags == null) {
            this.tags = "0";
        }

        else if (tags.isEmpty()) {
            this.tags = "0";
        }
        else{
            this.tags = tags;
        }



        if (postType == null) {
            throw new NullPointerException("Document postType cannot be null.");
        }

        if (postType.isEmpty()) {
            throw new IllegalStateException("Document postType cannot be empty.");
        }

        this.postType = postType;

        if (favouriteCount == null) {
            this.favouriteCount = "0";
        }
        else if (favouriteCount.isEmpty()) {
            this.favouriteCount = "0";
        }
        else{
            this.favouriteCount = favouriteCount;
        }

        if (formula_IDs == null) {
            throw new NullPointerException("Formula_IDs identifier cannot be null.");
        }
        else if (formula_IDs.isEmpty()) {
            // This is possible and in this case the arraylist has to stay empty
            this.formula_IDs = new ArrayList<>();
        }
        else{
            this.formula_IDs = formula_IDs;
        }


    }

    /**
     * Returns the unique document identifier.
     *
     * @return the unique document identifier.
     */
    public String getIdentifier() {
        return id;
    }

    /**
     * Returns the body of the document.
     *
     * @return the body of the document.
     */
    public String getBody() {
        return body;
    }

    public String getScore() {
        return score;
    }

    public String getViewCount() {
        return viewCount;
    }

    public String getTitle() {
        return title;
    }

    public String getTags() {
        return tags;
    }

    public String getPostType() {
        return postType;
    }

    public String getFavouriteCount() {
        return favouriteCount;
    }

    public ArrayList<Integer> getFormula_IDs() {
        return formula_IDs;
    }

    @Override
    public final String toString() {
        ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("identifier", id)
                .append("body", body)
                .append("score", score)
                .append("view count", viewCount)
                .append("title", title)
                .append("tags", tags)
                .append("post type", postType)
                .append("favourites count", favouriteCount)
                .append("formula_IDs", formula_IDs)
                ;

        return tsb.toString();
    }

    @Override
    public final boolean equals(Object o) {
        return (this == o) || ((o instanceof ParsedDocument) && id.equals(((ParsedDocument) o).id));
    }

    @Override
    public final int hashCode() {
        return 37 * id.hashCode();
    }


}
